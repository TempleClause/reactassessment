import Head from "next/head";
import Layout, { siteTitle } from "../components/layout";
import utilStyles from "../styles/utils.module.css";
import { GetStaticProps } from "next";

interface WeatherForecast {
  date: string;
  temperatureC: number;
  temperatureF: number;
  summary: string;
}

interface WeatherProps {
  weatherForecasts: WeatherForecast[];
}

export default function Weather({ weatherForecasts }: WeatherProps) {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>Weather Forecasts</h2>
        display the weather forecasts here
      </section>
    </Layout>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  const readWeatherForecasts = async () => {
    const response = await fetch("http://localhost:4000/weatherforecast");
    const result = ((await response.json()) as unknown) as WeatherForecast[];
    console.log(result);
    return result;
  };

  const weatherForecasts = await readWeatherForecasts();

  return {
    props: {
      weatherForecasts,
    },
  };
};
