# React Assessment

The project is split up into two parts: api and client. 
The api is based on the WeatherForecast WebAPI template and the client is based on the next js blog starter (written in TypeScript). 

## Required Software

The following list of software is needed to get the project up and running: 

- NodeJs:
  - https://nodejs.org/en/download/
- Yarn:
  - https://classic.yarnpkg.com/en/docs/install#windows-stable
- VSCode:
  - https://code.visualstudio.com/download
- dotnet 5 SDK:
  - https://dotnet.microsoft.com/download/dotnet/5.0



## How to get it running

- Open the root folder in VSCode
- Install the `ms-dotnettools.csharp` extension in VSCode. You might need to restart VSCode after this.
- Start the API:
  - On the left click on Run and Debug (ctrl + shift + D)
  - Select ".NET Core Launch (web)" in the dropdown
  - Click the green arrow next to the dropdown.
  - The API should getting started, visit http://localhost:4000/weatherforecast to confirm that the API is running
- Start the client:
  - Right click on the click folder -> open in integrated terminal
  - Run `yarn` to install all the packages
  - Run `yarn dev` to start the application
  - Visist http://localhost:3000/ to confirm that the client is running



## Tasks

### Self Introduction

Replace the [Your Self Introduction] with a lorem ipsum text. 

### Extend WeatherForecast 

- Extend the WeatherForecast Model with a `Location` property which is of type `string`. 
-  Also extend the model in the `client/pages/weather.tsx` file. 
- In the `WeatherForecastController` `GET` endpoint assign the `Location` property based on the `Summary` Property. 
  - "Freezing", "Bracing", "Chilly", "Cool" -> "Switzerland"
  - "Mild", "Warm", "Balmy" -> "France"
  - "Hot", "Sweltering", "Scorching" -> "Spain"
- Display the WeatherForecasts on the client on http://localhost:3000/weather. The page file `client/pages/weather.tsx` already exists. Write an `<ul>` list and `li` items with the following format:
  - `<li>{Location}: {Summary} {TemperatureC}C<li>`

### Add parameter to WeatherForecastController

- Add an `int` parameter to the `GET` endpoint in the `WeatherForecastController`. Make sure the parameter is read from the route. So something like  http://localhost:4000/weatherforecast/10  
- This int parameter should decide how many WeatherForecast's are returned. 
- Edit the `/client/pages/weather.tsx` file so 15 weather forecasts are retrieved.
- Only display 5 of the 15 weather forecasts at first and add a read more button.
- When the read more button is clicked all 15 should be displayed
  - Hint: make use of a `useState` https://reactjs.org/docs/hooks-state.html

